const setMessage = function setMessage(state, message) {
  state.message = message;
};

const getMessages = function getMessages(state, messages) {
  state.messages = messages;
};

export default { setMessage, getMessages };
